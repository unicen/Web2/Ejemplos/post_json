(() => {

    function send(e){
        e.preventDefault()
        
        let formData = new FormData(this);

        var object = {};
        formData.forEach(function(value, key){
            object[key] = value;
        });
        var json = JSON.stringify(object);
 
        fetch('api/object', {
            method: 'POST',
            body: json,
        }).then(console.log);

    }

    const button = document.getElementById('my-form');
    button.addEventListener('submit', send);
})()

